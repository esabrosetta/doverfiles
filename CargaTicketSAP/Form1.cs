﻿using System.IO;
using OfficeOpenXml;
using System.Configuration;
using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using ExcelDataReader;
using System.Linq;

namespace CargaTicketSAP
{
    public partial class Form1 : Form
    {
        DataTable tbl = new DataTable();
        public Form1()
        {
            try
            {
                InitializeComponent();

                LeerArchivos();
                //GuardaBitacora();
                lbl_Status.Text = "Finished";
                var tmr = new System.Windows.Forms.Timer();
                tmr.Tick += delegate {
                    this.Close();
                };
                tmr.Interval = (int)TimeSpan.FromMinutes(1).TotalMilliseconds;
                tmr.Start();
            }
            catch(Exception x)
            {
                return;
            }
            
        }


        public static void LeerArchivos()
        {
            var direccion = "c:\\Loaded_Files\\";

            string[] dirs = Directory.GetFiles(@direccion);

            int cantidad = dirs.Length;
            
            for (var i =0; i<cantidad; i++)
            {
                var arregloCadena = dirs[i].ToString().Split('\\');
                var nombreArchivo = arregloCadena[2].ToString();
                var rutaArchivoFinal = dirs[i].ToString();

               
                string consString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

               



                DataTable tbl = new DataTable();
                switch (nombreArchivo)
                {
                    case "cts.xlsxx":
                        
                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        

                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Limpiamos las tablas
                                con.Open();
                                var query = "truncate table dbo.cts";  // cambio a abreviatura ya q antes estaba por nombre
                                var cmd = new SqlCommand(query, con);
                                con.Close();


                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.cts";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table

                                sqlBulkCopy.ColumnMappings.Add("date", "ctsDate");
                                sqlBulkCopy.ColumnMappings.Add("dept", "dept");
                                sqlBulkCopy.ColumnMappings.Add("cts_pd", "cts_pd");
                                sqlBulkCopy.ColumnMappings.Add("inc_pd", "inc_pd");
                                sqlBulkCopy.ColumnMappings.Add("total_pd", "total_pd");
                                sqlBulkCopy.ColumnMappings.Add("cts_today", "cts_today");
                                sqlBulkCopy.ColumnMappings.Add("inc_today", "inc_today");
                                sqlBulkCopy.ColumnMappings.Add("tot_today", "tot_today");
                                sqlBulkCopy.ColumnMappings.Add("cts_wk1", "cts_wk1");
                                sqlBulkCopy.ColumnMappings.Add("inc_wk1", "inc_wk1");
                                sqlBulkCopy.ColumnMappings.Add("total_wk1", "total_wk1");
                                sqlBulkCopy.ColumnMappings.Add("cts_wk2", "cts_wk2");
                                sqlBulkCopy.ColumnMappings.Add("inc_wk2", "inc_wk2");
                                sqlBulkCopy.ColumnMappings.Add("total_wk2", "total_wk2");
                                sqlBulkCopy.ColumnMappings.Add("cts_wk3", "cts_wk3");
                                sqlBulkCopy.ColumnMappings.Add("inc_wk3", "inc_wk3");
                                sqlBulkCopy.ColumnMappings.Add("total_wk3", "total_wk3");
                                sqlBulkCopy.ColumnMappings.Add("cts_wk4", "cts_wk4");
                                sqlBulkCopy.ColumnMappings.Add("inc_wk4", "inc_wk4");
                                sqlBulkCopy.ColumnMappings.Add("total_wk4", "total_wk4");
                                sqlBulkCopy.ColumnMappings.Add("cts_wk5", "cts_wk5");
                                sqlBulkCopy.ColumnMappings.Add("inc_wk5", "inc_wk5");
                                sqlBulkCopy.ColumnMappings.Add("total_wk5", "total_wk5");
                                sqlBulkCopy.ColumnMappings.Add("cts_total", "cts_total");
                                sqlBulkCopy.ColumnMappings.Add("inc_total", "inc_total");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();

                            }
                        }
                        break;
                    case "gross_requirements.xlsxx":
                        
                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.GrossRequirements";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.GrossRequirements";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table

                                
                                sqlBulkCopy.ColumnMappings.Add("short", "SHORT");
                                sqlBulkCopy.ColumnMappings.Add("MAKE_BUY", "MAKE_BUY");
                                sqlBulkCopy.ColumnMappings.Add("PLANNER", "PLANNER");
                                sqlBulkCopy.ColumnMappings.Add("SEQ", "SEQ");
                                sqlBulkCopy.ColumnMappings.Add("ITEM", "ITEM");
                                sqlBulkCopy.ColumnMappings.Add("DESCRIP", "DESCRIP");
                                sqlBulkCopy.ColumnMappings.Add("ALPHA", "ALPHA");
                                sqlBulkCopy.ColumnMappings.Add("STOCK", "STOCK");
                                sqlBulkCopy.ColumnMappings.Add("WIP", "WIP");
                                sqlBulkCopy.ColumnMappings.Add("ON_HAND", "ON_HAND");
                                sqlBulkCopy.ColumnMappings.Add("USED", "USED");
                                sqlBulkCopy.ColumnMappings.Add("PAST_DUE", "PAST_DUE");
                                sqlBulkCopy.ColumnMappings.Add("PD_SHRT", "PD_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("TODAY", "TODAY");
                                sqlBulkCopy.ColumnMappings.Add("TODAY_SHRT", "TODAY_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP1", "WKP1");
                                sqlBulkCopy.ColumnMappings.Add("WKP1_SHRT", "WKP1_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP2", "WKP2");
                                sqlBulkCopy.ColumnMappings.Add("WKP2_SHRT", "WKP2_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP3", "WKP3");
                                sqlBulkCopy.ColumnMappings.Add("WKP3_SHRT", "WKP3_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP4", "WKP4");
                                sqlBulkCopy.ColumnMappings.Add("WKP4_SHRT", "WKP4_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP5", "WKP5");
                                sqlBulkCopy.ColumnMappings.Add("WKP5_SHRT", "WKP5_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("TOTAL", "TOTAL");
                                sqlBulkCopy.ColumnMappings.Add("TOTAL_SHRT", "TOTAL_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("ON_ORD", "ON_ORD");
                                sqlBulkCopy.ColumnMappings.Add("DETAIL", "DETAIL");
                                sqlBulkCopy.ColumnMappings.Add("POS", "POS");
                                sqlBulkCopy.ColumnMappings.Add("VENDOR", "VENDOR");
                                sqlBulkCopy.ColumnMappings.Add("NEG_BAL", "NEG_BAL");
                                sqlBulkCopy.ColumnMappings.Add("PERIOD", "PERIOD");
                                sqlBulkCopy.ColumnMappings.Add("PLNR_BYR", "PLNR_BYR");
                                sqlBulkCopy.ColumnMappings.Add("STAMPTECH", "STAMPTECH");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "gross_requirements_stamptech.xlsxx":
                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.GrossRequirementsStamptech";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.GrossRequirementsStamptech";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("short", "SHORT");
                                sqlBulkCopy.ColumnMappings.Add("MAKE_BUY", "MAKE_BUY");
                                sqlBulkCopy.ColumnMappings.Add("PLANNER", "PLANNER");
                                sqlBulkCopy.ColumnMappings.Add("SEQ", "SEQ");
                                sqlBulkCopy.ColumnMappings.Add("ITEM", "ITEM");
                                sqlBulkCopy.ColumnMappings.Add("DESCRIP", "DESCRIP");
                                sqlBulkCopy.ColumnMappings.Add("ALPHA", "ALPHA");
                                sqlBulkCopy.ColumnMappings.Add("STOCK", "STOCK");
                                sqlBulkCopy.ColumnMappings.Add("WIP", "WIP");
                                sqlBulkCopy.ColumnMappings.Add("ON_HAND", "ON_HAND");
                                sqlBulkCopy.ColumnMappings.Add("USED", "USED");
                                sqlBulkCopy.ColumnMappings.Add("PAST_DUE", "PAST_DUE");
                                sqlBulkCopy.ColumnMappings.Add("PD_SHRT", "PD_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("TODAY", "TODAY");
                                sqlBulkCopy.ColumnMappings.Add("TODAY_SHRT", "TODAY_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP1", "WKP1");
                                sqlBulkCopy.ColumnMappings.Add("WKP1_SHRT", "WKP1_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP2", "WKP2");
                                sqlBulkCopy.ColumnMappings.Add("WKP2_SHRT", "WKP2_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP3", "WKP3");
                                sqlBulkCopy.ColumnMappings.Add("WKP3_SHRT", "WKP3_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP4", "WKP4");
                                sqlBulkCopy.ColumnMappings.Add("WKP4_SHRT", "WKP4_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("WKP5", "WKP5");
                                sqlBulkCopy.ColumnMappings.Add("WKP5_SHRT", "WKP5_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("TOTAL", "TOTAL");
                                sqlBulkCopy.ColumnMappings.Add("TOTAL_SHRT", "TOTAL_SHRT");
                                sqlBulkCopy.ColumnMappings.Add("ON_ORD", "ON_ORD");
                                sqlBulkCopy.ColumnMappings.Add("DETAIL", "DETAIL");
                                sqlBulkCopy.ColumnMappings.Add("POS", "POS");
                                sqlBulkCopy.ColumnMappings.Add("VENDOR", "VENDOR");
                                sqlBulkCopy.ColumnMappings.Add("NEG_BAL", "NEG_BAL");
                                sqlBulkCopy.ColumnMappings.Add("PERIOD", "PERIOD");
                                sqlBulkCopy.ColumnMappings.Add("PLNR_BYR", "PLNR_BYR");
                                sqlBulkCopy.ColumnMappings.Add("STAMPTECH", "STAMPTECH");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "SHORTAGES_DETAIL.xlsxx":
                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.ShortagesDetail";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.ShortagesDetail";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table

                                sqlBulkCopy.ColumnMappings.Add("MAKE_BUY", "MAKE_BUY");
                                sqlBulkCopy.ColumnMappings.Add("SEQ", "SEQ");
                                sqlBulkCopy.ColumnMappings.Add("DEPT", "DEPT");
                                sqlBulkCopy.ColumnMappings.Add("WO", "WO");
                                sqlBulkCopy.ColumnMappings.Add("INDX", "INDX");
                                sqlBulkCopy.ColumnMappings.Add("COMMENTS", "COMMENTS");
                                sqlBulkCopy.ColumnMappings.Add("LOAD_DATE", "LOAD_DATE");
                                sqlBulkCopy.ColumnMappings.Add("DUE_DATE", "DUE_DATE");
                                sqlBulkCopy.ColumnMappings.Add("SEQ_NO", "SEQ_NO");
                                sqlBulkCopy.ColumnMappings.Add("BASE_MODEL", "BASE_MODEL");
                                sqlBulkCopy.ColumnMappings.Add("DESCRIP_A", "DESCRIP_A");
                                sqlBulkCopy.ColumnMappings.Add("PARENT_PRO", "PARENT_PRO");
                                sqlBulkCopy.ColumnMappings.Add("CUST_NAME", "CUST_NAME");
                                sqlBulkCopy.ColumnMappings.Add("ITEM", "ITEM");
                                sqlBulkCopy.ColumnMappings.Add("DESCRIP_B", "DESCRIP_B");
                                sqlBulkCopy.ColumnMappings.Add("ALPHA", "ALPHA");
                                sqlBulkCopy.ColumnMappings.Add("SHP_WH", "SHP_WH");
                                sqlBulkCopy.ColumnMappings.Add("PLANNER", "PLANNER");
                                sqlBulkCopy.ColumnMappings.Add("REQ_QTY", "REQ_QTY");
                                sqlBulkCopy.ColumnMappings.Add("ISSUE_QTY", "ISSUE_QTY");
                                sqlBulkCopy.ColumnMappings.Add("REQ_DATE", "REQ_DATE");
                                sqlBulkCopy.ColumnMappings.Add("STATION", "STATION");
                                sqlBulkCopy.ColumnMappings.Add("OP_DESC", "OP_DESC");
                                sqlBulkCopy.ColumnMappings.Add("SUPPLY", "SUPPLY");
                                sqlBulkCopy.ColumnMappings.Add("ALLOC_QTY", "ALLOC_QTY");
                                sqlBulkCopy.ColumnMappings.Add("SUPPL_DT", "SUPPL_DT");
                                sqlBulkCopy.ColumnMappings.Add("SUPPL_QTY", "SUPPL_QTY");
                                sqlBulkCopy.ColumnMappings.Add("SUPPL_ORD", "SUPPL_ORD");
                                sqlBulkCopy.ColumnMappings.Add("SUPPLIER", "SUPPLIER");
                                sqlBulkCopy.ColumnMappings.Add("PERIOD", "PERIOD");
                                sqlBulkCopy.ColumnMappings.Add("STAMPTECH", "STAMPTECH");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "shortages_priorities.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.ShortagesPriorities";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.ShortagesPriorities";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table

                                sqlBulkCopy.ColumnMappings.Add("make_buy", "make_buy");
                                sqlBulkCopy.ColumnMappings.Add("planner", "planner");
                                sqlBulkCopy.ColumnMappings.Add("seq", "seq");
                                sqlBulkCopy.ColumnMappings.Add("item", "item");
                                sqlBulkCopy.ColumnMappings.Add("descrip", "descrip");
                                sqlBulkCopy.ColumnMappings.Add("alpha", "alpha");
                                sqlBulkCopy.ColumnMappings.Add("comment", "comment");
                                sqlBulkCopy.ColumnMappings.Add("past_due_a", "past_due_a");
                                sqlBulkCopy.ColumnMappings.Add("today_a", "today_a");
                                sqlBulkCopy.ColumnMappings.Add("wkp1", "wkp1");
                                sqlBulkCopy.ColumnMappings.Add("wkp2", "wkp2");
                                sqlBulkCopy.ColumnMappings.Add("wkp3", "wkp3");
                                sqlBulkCopy.ColumnMappings.Add("wkp4", "wkp4");
                                sqlBulkCopy.ColumnMappings.Add("wkp5", "wkp5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                                sqlBulkCopy.ColumnMappings.Add("neg_bal", "neg_bal");
                                sqlBulkCopy.ColumnMappings.Add("on_ord", "on_ord");
                                sqlBulkCopy.ColumnMappings.Add("detail", "detail");
                                sqlBulkCopy.ColumnMappings.Add("pos", "pos");
                                sqlBulkCopy.ColumnMappings.Add("vendor", "vendor");
                                sqlBulkCopy.ColumnMappings.Add("period", "period");
                                sqlBulkCopy.ColumnMappings.Add("plnr_byr", "plnr_byr");
                                sqlBulkCopy.ColumnMappings.Add("stamptech", "stamptech");
                                sqlBulkCopy.ColumnMappings.Add("l124", "l124");
                                sqlBulkCopy.ColumnMappings.Add("past_due_b", "past_due_b");
                                sqlBulkCopy.ColumnMappings.Add("today_b", "today_b");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("imp_wk1", "imp_wk1");
                                sqlBulkCopy.ColumnMappings.Add("total_imp", "total_imp");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "shortages_priorities_stamptech.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.ShortagesPrioritiesStamptech";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.ShortagesPrioritiesStamptech";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table


                                sqlBulkCopy.ColumnMappings.Add("make_buy", "make_buy");
                                sqlBulkCopy.ColumnMappings.Add("planner", "planner");
                                sqlBulkCopy.ColumnMappings.Add("seq", "seq");
                                sqlBulkCopy.ColumnMappings.Add("item", "item");
                                sqlBulkCopy.ColumnMappings.Add("descrip", "descrip");
                                sqlBulkCopy.ColumnMappings.Add("alpha", "alpha");
                                sqlBulkCopy.ColumnMappings.Add("comment", "comment");
                                sqlBulkCopy.ColumnMappings.Add("past_due_a", "past_due_a");
                                sqlBulkCopy.ColumnMappings.Add("today_a", "today_a");
                                sqlBulkCopy.ColumnMappings.Add("wkp1", "wkp1");
                                sqlBulkCopy.ColumnMappings.Add("wkp2", "wkp2");
                                sqlBulkCopy.ColumnMappings.Add("wkp3", "wkp3");
                                sqlBulkCopy.ColumnMappings.Add("wkp4", "wkp4");
                                sqlBulkCopy.ColumnMappings.Add("wkp5", "wkp5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                                sqlBulkCopy.ColumnMappings.Add("neg_bal", "neg_bal");
                                sqlBulkCopy.ColumnMappings.Add("on_ord", "on_ord");
                                sqlBulkCopy.ColumnMappings.Add("detail", "detail");
                                sqlBulkCopy.ColumnMappings.Add("pos", "pos");
                                sqlBulkCopy.ColumnMappings.Add("vendor", "vendor");
                                sqlBulkCopy.ColumnMappings.Add("period", "period");
                                sqlBulkCopy.ColumnMappings.Add("plnr_byr", "plnr_byr");
                                sqlBulkCopy.ColumnMappings.Add("stamptech", "stamptech");
                                sqlBulkCopy.ColumnMappings.Add("l124", "l124");
                                sqlBulkCopy.ColumnMappings.Add("past_due_b", "past_due_b");
                                sqlBulkCopy.ColumnMappings.Add("today_b", "today_b");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("imp_wk1", "imp_wk1");
                                sqlBulkCopy.ColumnMappings.Add("total_imp", "total_imp");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "summary.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.summary";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.summary";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("SEQ_PR", "SEQ_PR");
                                sqlBulkCopy.ColumnMappings.Add("SEQ_NO", "SEQ_NO");
                                sqlBulkCopy.ColumnMappings.Add("DEPMNT", "DEPMNT");
                                sqlBulkCopy.ColumnMappings.Add("ORDER", "ORDER");
                                sqlBulkCopy.ColumnMappings.Add("LINEUP", "LINEUP");
                                sqlBulkCopy.ColumnMappings.Add("STATUS", "STATUS");
                                sqlBulkCopy.ColumnMappings.Add("STATUS_BO", "STATUS_BO");
                                sqlBulkCopy.ColumnMappings.Add("CASE", "CASE");
                                sqlBulkCopy.ColumnMappings.Add("WO", "WO");
                                sqlBulkCopy.ColumnMappings.Add("COMMENT", "COMMENT");
                                sqlBulkCopy.ColumnMappings.Add("STAT", "STAT");
                                sqlBulkCopy.ColumnMappings.Add("STAT_SUB", "STAT_SUB");
                                sqlBulkCopy.ColumnMappings.Add("STAT_BOTH", "STAT_BOTH");
                                sqlBulkCopy.ColumnMappings.Add("DUE_DATE", "DUE_DATE");
                                sqlBulkCopy.ColumnMappings.Add("REQ_DATE", "REQ_DATE");
                                sqlBulkCopy.ColumnMappings.Add("ETA", "ETA");
                                sqlBulkCopy.ColumnMappings.Add("DUE", "DUE");
                                sqlBulkCopy.ColumnMappings.Add("LATE", "LATE");
                                sqlBulkCopy.ColumnMappings.Add("PL", "PL");
                                sqlBulkCopy.ColumnMappings.Add("ITEM", "ITEM");
                                sqlBulkCopy.ColumnMappings.Add("DESCRIP", "DESCRIP");
                                sqlBulkCopy.ColumnMappings.Add("M_OH", "M_OH");
                                sqlBulkCopy.ColumnMappings.Add("M_PL", "M_PL");
                                sqlBulkCopy.ColumnMappings.Add("B_OH", "B_OH");
                                sqlBulkCopy.ColumnMappings.Add("B_PO", "B_PO");
                                sqlBulkCopy.ColumnMappings.Add("B_PL", "B_PL");
                                sqlBulkCopy.ColumnMappings.Add("TC_REQ", "TC_REQ");
                                sqlBulkCopy.ColumnMappings.Add("TC_STAT", "TC_STAT");
                                sqlBulkCopy.ColumnMappings.Add("TC_M", "TC_M");
                                sqlBulkCopy.ColumnMappings.Add("TC_B", "TC_B");
                                sqlBulkCopy.ColumnMappings.Add("COIL_REQ", "COIL_REQ");
                                sqlBulkCopy.ColumnMappings.Add("COIL_STAT", "COIL_STAT");
                                sqlBulkCopy.ColumnMappings.Add("COIL_M", "COIL_M");
                                sqlBulkCopy.ColumnMappings.Add("COIL_B", "COIL_B");
                                sqlBulkCopy.ColumnMappings.Add("LE_REQ", "LE_REQ");
                                sqlBulkCopy.ColumnMappings.Add("LE_STAT", "LE_STAT");
                                sqlBulkCopy.ColumnMappings.Add("L_M", "L_M");
                                sqlBulkCopy.ColumnMappings.Add("L_B", "L_B");
                                sqlBulkCopy.ColumnMappings.Add("RE_REQ", "RE_REQ");
                                sqlBulkCopy.ColumnMappings.Add("RE_STAT", "RE_STAT");
                                sqlBulkCopy.ColumnMappings.Add("R_M", "R_M");
                                sqlBulkCopy.ColumnMappings.Add("R_B", "R_B");
                                sqlBulkCopy.ColumnMappings.Add("PE_REQ", "PE_REQ");
                                sqlBulkCopy.ColumnMappings.Add("PE_STAT", "PE_STAT");
                                sqlBulkCopy.ColumnMappings.Add("P_M", "P_M");
                                sqlBulkCopy.ColumnMappings.Add("P_B", "P_B");
                                sqlBulkCopy.ColumnMappings.Add("SW_REQ", "SW_REQ");
                                sqlBulkCopy.ColumnMappings.Add("SW_STAT", "SW_STAT");
                                sqlBulkCopy.ColumnMappings.Add("PNT_REQ", "PNT_REQ");
                                sqlBulkCopy.ColumnMappings.Add("PNT_STAT", "PNT_STAT");
                                sqlBulkCopy.ColumnMappings.Add("WLD_REQ", "WLD_REQ");
                                sqlBulkCopy.ColumnMappings.Add("WLD_STAT", "WLD_STAT");
                                sqlBulkCopy.ColumnMappings.Add("CJ_REQ", "CJ_REQ");
                                sqlBulkCopy.ColumnMappings.Add("CJ_STAT", "CJ_STAT");
                                sqlBulkCopy.ColumnMappings.Add("CJ_M", "CJ_M");
                                sqlBulkCopy.ColumnMappings.Add("CJ_B", "CJ_B");
                                sqlBulkCopy.ColumnMappings.Add("SHLV_REQ", "SHLV_REQ");
                                sqlBulkCopy.ColumnMappings.Add("SHLV_STAT", "SHLV_STAT");
                                sqlBulkCopy.ColumnMappings.Add("SHLV_M", "SHLV_M");
                                sqlBulkCopy.ColumnMappings.Add("SHLV_B", "SHLV_B");
                                sqlBulkCopy.ColumnMappings.Add("ELEC_REQ", "ELEC_REQ");
                                sqlBulkCopy.ColumnMappings.Add("ELEC_STAT", "ELEC_STAT");
                                sqlBulkCopy.ColumnMappings.Add("ELEC_M", "ELEC_M");
                                sqlBulkCopy.ColumnMappings.Add("ELEC_B", "ELEC_B");
                                sqlBulkCopy.ColumnMappings.Add("OTHER_M", "OTHER_M");
                                sqlBulkCopy.ColumnMappings.Add("OTHER_B", "OTHER_B");
                                sqlBulkCopy.ColumnMappings.Add("CUSTOMER", "CUSTOMER");
                                sqlBulkCopy.ColumnMappings.Add("MAT_REQ", "MAT_REQ");
                                sqlBulkCopy.ColumnMappings.Add("HOST_DT", "HOST_DT");
                                sqlBulkCopy.ColumnMappings.Add("PERIOD", "PERIOD");
                                sqlBulkCopy.ColumnMappings.Add("DAILY_P", "DAILY_P");
                                sqlBulkCopy.ColumnMappings.Add("HORIZON", "HORIZON");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "cts_h.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.cts_h";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.cts_h";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("dep", "dep");
                                sqlBulkCopy.ColumnMappings.Add("pd_c", "pd_c");
                                sqlBulkCopy.ColumnMappings.Add("pd_i", "pd_i");
                                sqlBulkCopy.ColumnMappings.Add("pd_t", "pd_t");
                                sqlBulkCopy.ColumnMappings.Add("d1_c", "d1_c");
                                sqlBulkCopy.ColumnMappings.Add("d1_i", "d1_i");
                                sqlBulkCopy.ColumnMappings.Add("d1_t", "d1_t");
                                sqlBulkCopy.ColumnMappings.Add("d2_c", "d2_c");
                                sqlBulkCopy.ColumnMappings.Add("d2_i", "d2_i");
                                sqlBulkCopy.ColumnMappings.Add("d2_t", "d2_t");
                                sqlBulkCopy.ColumnMappings.Add("d3_c", "d3_c");
                                sqlBulkCopy.ColumnMappings.Add("d3_i", "d3_i");
                                sqlBulkCopy.ColumnMappings.Add("d3_t", "d3_t");
                                sqlBulkCopy.ColumnMappings.Add("d4_c", "d4_c");
                                sqlBulkCopy.ColumnMappings.Add("d4_i", "d4_i");
                                sqlBulkCopy.ColumnMappings.Add("d4_t", "d4_t");
                                sqlBulkCopy.ColumnMappings.Add("d5_c", "d5_c");
                                sqlBulkCopy.ColumnMappings.Add("d5_i", "d5_i");
                                sqlBulkCopy.ColumnMappings.Add("d5_t", "d5_t");
                                sqlBulkCopy.ColumnMappings.Add("d6_c", "d6_c");
                                sqlBulkCopy.ColumnMappings.Add("d6_i", "d6_i");
                                sqlBulkCopy.ColumnMappings.Add("d6_t", "d6_t");
                                sqlBulkCopy.ColumnMappings.Add("d7_c", "d7_c");
                                sqlBulkCopy.ColumnMappings.Add("d7_i", "d7_i");
                                sqlBulkCopy.ColumnMappings.Add("d7_t", "d7_t");
                                sqlBulkCopy.ColumnMappings.Add("d8_c", "d8_c");
                                sqlBulkCopy.ColumnMappings.Add("d8_i", "d8_i");
                                sqlBulkCopy.ColumnMappings.Add("d8_t", "d8_t");
                                sqlBulkCopy.ColumnMappings.Add("d9_c", "d9_c");
                                sqlBulkCopy.ColumnMappings.Add("d9_i", "d9_i");
                                sqlBulkCopy.ColumnMappings.Add("d9_t", "d9_t");
                                sqlBulkCopy.ColumnMappings.Add("d10_c", "d10_c");
                                sqlBulkCopy.ColumnMappings.Add("d10_i", "d10_i");
                                sqlBulkCopy.ColumnMappings.Add("d10_t", "d10_t");
                                sqlBulkCopy.ColumnMappings.Add("wk3_c", "wk3_c");
                                sqlBulkCopy.ColumnMappings.Add("wk3_i", "wk3_i");
                                sqlBulkCopy.ColumnMappings.Add("wk3_t", "wk3_t");
                                sqlBulkCopy.ColumnMappings.Add("wk4_c", "wk4_c");
                                sqlBulkCopy.ColumnMappings.Add("wk4_i", "wk4_i");
                                sqlBulkCopy.ColumnMappings.Add("wk4_t", "wk4_t");
                                sqlBulkCopy.ColumnMappings.Add("wk5_c", "wk5_c");
                                sqlBulkCopy.ColumnMappings.Add("wk5_i", "wk5_i");
                                sqlBulkCopy.ColumnMappings.Add("wk5_t", "wk5_t");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "cts_summary.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.cts_summary";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.cts_summary";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("date", "date");
                                sqlBulkCopy.ColumnMappings.Add("cts", "cts");
                                
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "exclusion_list.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.exclusion_list";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.exclusion_list";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("item", "item");
                                
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "exclusion_list_strc.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.exclusion_list_strc";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.exclusion_list_strc";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("field_name", "field_name");
                                sqlBulkCopy.ColumnMappings.Add("field_type", "field_type");
                                sqlBulkCopy.ColumnMappings.Add("field_len", "field_len");
                                sqlBulkCopy.ColumnMappings.Add("field_dec", "field_dec");
                                sqlBulkCopy.ColumnMappings.Add("field_null", "field_null");
                                sqlBulkCopy.ColumnMappings.Add("field_nocp", "field_nocp");
                                sqlBulkCopy.ColumnMappings.Add("table_name", "table_name");
                                sqlBulkCopy.ColumnMappings.Add("field_next", "field_next");
                                sqlBulkCopy.ColumnMappings.Add("field_step", "field_step");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_buy.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_buy";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_buy";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                               
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_make.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_make";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_make";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_negatives_buy.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_negatives_buy";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_negatives_buy";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_negatives_make.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_negatives_make";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_negatives_make";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_stamptech_buy.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_stamptech_buy";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_stamptech_buy";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "metric_shortages_stamptech_negatives_buy.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.metric_shortages_stamptech_negatives_buy";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.metric_shortages_stamptech_negatives_buy";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("name", "name");
                                sqlBulkCopy.ColumnMappings.Add("pd", "pd");
                                sqlBulkCopy.ColumnMappings.Add("today", "today");
                                sqlBulkCopy.ColumnMappings.Add("wk1", "wk1");
                                sqlBulkCopy.ColumnMappings.Add("wk2", "wk2");
                                sqlBulkCopy.ColumnMappings.Add("wk3", "wk3");
                                sqlBulkCopy.ColumnMappings.Add("wk4", "wk4");
                                sqlBulkCopy.ColumnMappings.Add("wk5", "wk5");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "shortages_negatives_summary_history.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.shortages_negatives_summary_history";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.shortages_negatives_summary_history";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("date", "date");
                                sqlBulkCopy.ColumnMappings.Add("buy_wk1", "buy_wk1");
                                sqlBulkCopy.ColumnMappings.Add("buy_all", "buy_all");
                                sqlBulkCopy.ColumnMappings.Add("make_wk1", "make_wk1");
                                sqlBulkCopy.ColumnMappings.Add("make_all", "make_all");
                                sqlBulkCopy.ColumnMappings.Add("total_wk1", "total_wk1");
                                sqlBulkCopy.ColumnMappings.Add("total_all", "total_all");
                                

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "shortages_summary_history.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.shortages_summary_history";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.shortages_summary_history";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("date", "date");
                                sqlBulkCopy.ColumnMappings.Add("buy_wk1", "buy_wk1");
                                sqlBulkCopy.ColumnMappings.Add("buy_wk2", "buy_wk2");
                                sqlBulkCopy.ColumnMappings.Add("make_wk1", "make_wk1");
                                sqlBulkCopy.ColumnMappings.Add("make_wk2", "make_wk2");
                               
                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "subassys.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.subassys";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.subassys";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("station", "station");
                                sqlBulkCopy.ColumnMappings.Add("pd_c", "pd_c");
                                sqlBulkCopy.ColumnMappings.Add("pd_i", "pd_i");
                                sqlBulkCopy.ColumnMappings.Add("pd_t", "pd_t");
                                sqlBulkCopy.ColumnMappings.Add("d1_c", "d1_c");
                                sqlBulkCopy.ColumnMappings.Add("d1_i", "d1_i");
                                sqlBulkCopy.ColumnMappings.Add("d1_t", "d1_t");
                                sqlBulkCopy.ColumnMappings.Add("d2_c", "d2_c");
                                sqlBulkCopy.ColumnMappings.Add("d2_i", "d2_i");
                                sqlBulkCopy.ColumnMappings.Add("d2_t", "d2_t");
                                sqlBulkCopy.ColumnMappings.Add("d3_c", "d3_c");
                                sqlBulkCopy.ColumnMappings.Add("d3_i", "d3_i");
                                sqlBulkCopy.ColumnMappings.Add("d3_t", "d3_t");
                                sqlBulkCopy.ColumnMappings.Add("d4_c", "d4_c");
                                sqlBulkCopy.ColumnMappings.Add("d4_i", "d4_i");
                                sqlBulkCopy.ColumnMappings.Add("d4_t", "d4_t");
                                sqlBulkCopy.ColumnMappings.Add("d5_c", "d5_c");
                                sqlBulkCopy.ColumnMappings.Add("d5_i", "d5_i");
                                sqlBulkCopy.ColumnMappings.Add("d5_t", "d5_t");
                                sqlBulkCopy.ColumnMappings.Add("d6_c", "d6_c");
                                sqlBulkCopy.ColumnMappings.Add("d6_i", "d6_i");
                                sqlBulkCopy.ColumnMappings.Add("d6_t", "d6_t");
                                sqlBulkCopy.ColumnMappings.Add("d7_c", "d7_c");
                                sqlBulkCopy.ColumnMappings.Add("d7_i", "d7_i");
                                sqlBulkCopy.ColumnMappings.Add("d7_t", "d7_t");
                                sqlBulkCopy.ColumnMappings.Add("d8_c", "d8_c");
                                sqlBulkCopy.ColumnMappings.Add("d8_i", "d8_i");
                                sqlBulkCopy.ColumnMappings.Add("d8_t", "d8_t");
                                sqlBulkCopy.ColumnMappings.Add("d9_c", "d9_c");
                                sqlBulkCopy.ColumnMappings.Add("d9_i", "d9_i");
                                sqlBulkCopy.ColumnMappings.Add("d9_t", "d9_t");
                                sqlBulkCopy.ColumnMappings.Add("d10_c", "d10_c");
                                sqlBulkCopy.ColumnMappings.Add("d10_i", "d10_i");
                                sqlBulkCopy.ColumnMappings.Add("d10_t", "d10_t");
                                sqlBulkCopy.ColumnMappings.Add("wk3_c", "wk3_c");
                                sqlBulkCopy.ColumnMappings.Add("wk3_i", "wk3_i");
                                sqlBulkCopy.ColumnMappings.Add("wk3_t", "wk3_t");
                                sqlBulkCopy.ColumnMappings.Add("wk4_c", "wk4_c");
                                sqlBulkCopy.ColumnMappings.Add("wk4_i", "wk4_i");
                                sqlBulkCopy.ColumnMappings.Add("wk4_t", "wk4_t");
                                sqlBulkCopy.ColumnMappings.Add("wk5_c", "wk5_c");
                                sqlBulkCopy.ColumnMappings.Add("wk5_i", "wk5_i");
                                sqlBulkCopy.ColumnMappings.Add("wk5_t", "wk5_t");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");
                              

                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;

                    case "subassys_124.xlsxX":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.subassys_124";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.subassys_124";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("station", "station");
                                sqlBulkCopy.ColumnMappings.Add("pd_c", "pd_c");
                                sqlBulkCopy.ColumnMappings.Add("pd_i", "pd_i");
                                sqlBulkCopy.ColumnMappings.Add("pd_t", "pd_t");
                                sqlBulkCopy.ColumnMappings.Add("d1_c", "d1_c");
                                sqlBulkCopy.ColumnMappings.Add("d1_i", "d1_i");
                                sqlBulkCopy.ColumnMappings.Add("d1_t", "d1_t");
                                sqlBulkCopy.ColumnMappings.Add("d2_c", "d2_c");
                                sqlBulkCopy.ColumnMappings.Add("d2_i", "d2_i");
                                sqlBulkCopy.ColumnMappings.Add("d2_t", "d2_t");
                                sqlBulkCopy.ColumnMappings.Add("d3_c", "d3_c");
                                sqlBulkCopy.ColumnMappings.Add("d3_i", "d3_i");
                                sqlBulkCopy.ColumnMappings.Add("d3_t", "d3_t");
                                sqlBulkCopy.ColumnMappings.Add("d4_c", "d4_c");
                                sqlBulkCopy.ColumnMappings.Add("d4_i", "d4_i");
                                sqlBulkCopy.ColumnMappings.Add("d4_t", "d4_t");
                                sqlBulkCopy.ColumnMappings.Add("d5_c", "d5_c");
                                sqlBulkCopy.ColumnMappings.Add("d5_i", "d5_i");
                                sqlBulkCopy.ColumnMappings.Add("d5_t", "d5_t");
                                sqlBulkCopy.ColumnMappings.Add("d6_c", "d6_c");
                                sqlBulkCopy.ColumnMappings.Add("d6_i", "d6_i");
                                sqlBulkCopy.ColumnMappings.Add("d6_t", "d6_t");
                                sqlBulkCopy.ColumnMappings.Add("d7_c", "d7_c");
                                sqlBulkCopy.ColumnMappings.Add("d7_i", "d7_i");
                                sqlBulkCopy.ColumnMappings.Add("d7_t", "d7_t");
                                sqlBulkCopy.ColumnMappings.Add("d8_c", "d8_c");
                                sqlBulkCopy.ColumnMappings.Add("d8_i", "d8_i");
                                sqlBulkCopy.ColumnMappings.Add("d8_t", "d8_t");
                                sqlBulkCopy.ColumnMappings.Add("d9_c", "d9_c");
                                sqlBulkCopy.ColumnMappings.Add("d9_i", "d9_i");
                                sqlBulkCopy.ColumnMappings.Add("d9_t", "d9_t");
                                sqlBulkCopy.ColumnMappings.Add("d10_c", "d10_c");
                                sqlBulkCopy.ColumnMappings.Add("d10_i", "d10_i");
                                sqlBulkCopy.ColumnMappings.Add("d10_t", "d10_t");
                                sqlBulkCopy.ColumnMappings.Add("wk3_c", "wk3_c");
                                sqlBulkCopy.ColumnMappings.Add("wk3_i", "wk3_i");
                                sqlBulkCopy.ColumnMappings.Add("wk3_t", "wk3_t");
                                sqlBulkCopy.ColumnMappings.Add("wk4_c", "wk4_c");
                                sqlBulkCopy.ColumnMappings.Add("wk4_i", "wk4_i");
                                sqlBulkCopy.ColumnMappings.Add("wk4_t", "wk4_t");
                                sqlBulkCopy.ColumnMappings.Add("wk5_c", "wk5_c");
                                sqlBulkCopy.ColumnMappings.Add("wk5_i", "wk5_i");
                                sqlBulkCopy.ColumnMappings.Add("wk5_t", "wk5_t");
                                sqlBulkCopy.ColumnMappings.Add("total", "total");


                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "PICK_LIST_SUBASSY.xlsx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.pegging";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.pegging";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("dept", "dept");
                                sqlBulkCopy.ColumnMappings.Add("seq", "seq");
                                sqlBulkCopy.ColumnMappings.Add("seq_no", "seq_no");
                                sqlBulkCopy.ColumnMappings.Add("due_date", "due_date");
                                sqlBulkCopy.ColumnMappings.Add("op_seq", "op_seq");
                                sqlBulkCopy.ColumnMappings.Add("station", "station");
                                sqlBulkCopy.ColumnMappings.Add("wo", "wo");
                                sqlBulkCopy.ColumnMappings.Add("item", "item");
                                sqlBulkCopy.ColumnMappings.Add("item_desc", "item_desc");
                                sqlBulkCopy.ColumnMappings.Add("shp_wh", "shp_wh");
                                sqlBulkCopy.ColumnMappings.Add("planner", "planner");
                                sqlBulkCopy.ColumnMappings.Add("req_qty", "req_qty");
                                sqlBulkCopy.ColumnMappings.Add("req_date", "req_date");
                                sqlBulkCopy.ColumnMappings.Add("mrp_date", "mrp_date");
                                sqlBulkCopy.ColumnMappings.Add("color", "color");
                                


                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                    case "PICK_LIST_FINAL_ASSY.xlsxx":

                        using (var pck = new ExcelPackage(new System.IO.FileInfo(rutaArchivoFinal)))
                        {
                            using (var stream = File.OpenRead(rutaArchivoFinal))
                            {
                                pck.Load(stream);
                            }
                            var ws = pck.Workbook.Worksheets.First();

                            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            {
                                tbl.Columns.Add(firstRowCell.Text);
                            }
                            var startRow = 2;
                            for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                            {
                                var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                                DataRow row = tbl.Rows.Add();
                                foreach (var cell in wsRow)
                                {
                                    row[cell.Start.Column - 1] = cell.Value;
                                }
                            }
                        }
                        using (SqlConnection con = new SqlConnection(consString))
                        {
                            //Limpiamos las tablas
                            con.Open();
                            var query = "truncate table dbo.pegging";  // cambio a abreviatura ya q antes estaba por nombre
                            var cmd = new SqlCommand(query, con);
                            con.Close();
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {
                                //Set the database table name
                                sqlBulkCopy.DestinationTableName = "dbo.pegging";

                                ////[OPTIONAL]: Map the Excel columns with that of the database table
                                sqlBulkCopy.ColumnMappings.Add("dept", "dept");
                                sqlBulkCopy.ColumnMappings.Add("seq", "seq");
                                sqlBulkCopy.ColumnMappings.Add("seq_no", "seq_no");
                                sqlBulkCopy.ColumnMappings.Add("due_date", "due_date");
                                sqlBulkCopy.ColumnMappings.Add("op_seq", "op_seq");
                                sqlBulkCopy.ColumnMappings.Add("station", "station");
                                sqlBulkCopy.ColumnMappings.Add("wo", "wo");
                                sqlBulkCopy.ColumnMappings.Add("item", "item");
                                sqlBulkCopy.ColumnMappings.Add("item_description", "item_desc");
                                sqlBulkCopy.ColumnMappings.Add("shp_wh", "shp_wh");
                                sqlBulkCopy.ColumnMappings.Add("planner", "planner");
                                sqlBulkCopy.ColumnMappings.Add("req_qty", "req_qty");
                                sqlBulkCopy.ColumnMappings.Add("req_date", "req_date");
                                sqlBulkCopy.ColumnMappings.Add("mrp_date", "mrp_date");
                                sqlBulkCopy.ColumnMappings.Add("color", "color");



                                con.Open();
                                sqlBulkCopy.BulkCopyTimeout = 0;
                                sqlBulkCopy.WriteToServer(tbl);
                                con.Close();
                            }
                        }
                        break;
                }

            }

        }

        //public void MueveArchivo(string archivo)
        //{
        //    var direccion = "c:\\Loaded_Files\\"+archivo;
        //    var direccionDestino = "c:\\Loaded_Files\\fileUpload_" + DateTime.Now.Month+"_"+ DateTime.Now.Day +"_"+ DateTime.Now.Year+"_"+DateTime.Now.Second+"_"+archivo+".txt";
        //    System.IO.File.Move(direccion, direccionDestino);
        //    lbl_Status.Text = "File Loaded";
        //}

        //public static void DeleteFileFTP(string RemoteFile, string Login, string Password)
        //{
        //    FtpWebRequest requestFileDelete = (FtpWebRequest)FtpWebRequest.Create(RemoteFile);
        //    requestFileDelete.Credentials = new NetworkCredential(Login, Password);
        //    requestFileDelete.Method = WebRequestMethods.Ftp.DeleteFile;

        //    FtpWebResponse responseFileDelete = (FtpWebResponse)requestFileDelete.GetResponse();
        //}

        ////public void GuardaBitacora()
        ////{
        ////    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        ////    conn.Open();
        ////    var fecha = DateTime.Now.ToShortDateString();
        ////    var query = "insert into bitacoraCargaSAP select '"+fecha+"'";
        ////    var cmd1 = new SqlCommand(query, conn);
        ////    cmd1.ExecuteReader();

        ////    var query = "select id from bitacoraCargaSAP where fechaCarga='" +fecha+ "'";
        ////    var cmd1 = new SqlCommand(query, conn);
        ////    cmd1.ExecuteReader();


        ////    conn.Close();
        ////}
        //public void CargaArchivo()
        //{
        //    try
        //    {
        //        var dia = DateTime.Now.Day.ToString();
        //        dia = (dia.Length == 1) ? dia = '0' + dia : dia;

        //        var mes = DateTime.Now.Month.ToString();
        //        mes = (mes.Length == 1) ? mes = '0' + mes : mes;

        //        var año = DateTime.Now.Year.ToString();



        //        var directorioFtp = "ftp://13.68.85.176//Files";
        //        var user = "doverfood"; //rosetta   HUhsj8w99wiq89    rosettaQA   HUXXu6738w99
        //        var pass = @"Ana0309Max#21";

        //        var archivo = "";
        //        // Obtiene el objeto que se utiliza para comunicarse con el servidor.
        //        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(directorioFtp);
        //        request.Method = WebRequestMethods.Ftp.ListDirectory;

        //        // Este ejemplo asume que el sitio FTP utiliza autenticación anónima.
        //        request.Credentials = new NetworkCredential(user, pass);

        //        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        //        StreamReader streamReader = new StreamReader(response.GetResponseStream());

        //        List<string> directories = new List<string>();

        //        string line = streamReader.ReadLine();

        //        if (string.IsNullOrEmpty(line))
        //        {
        //            lbl_FileName.Text = "No such file";
        //            lbl_Status.Text = "Finished";
        //            var tmr = new System.Windows.Forms.Timer();
        //            tmr.Tick += delegate {
        //                this.Close();
        //            };
        //            tmr.Interval = (int)TimeSpan.FromMinutes(1).TotalMilliseconds;
        //            tmr.Start();
        //            return;
        //        }
        //        //Obtiene el contenido y lo agrega al List<string>.
        //        while (!string.IsNullOrEmpty(line))
        //        {
        //            archivo = line;
        //            line = streamReader.ReadLine();
        //            var archivoF = directorioFtp + "//" + archivo;
        //            var destino = "c:/Loaded_Files/";

        //            DownloadFTP(destino, archivoF, user, pass);
        //            DeleteFileFTP(archivoF, user, pass);

        //            lbl_FileName.Text = archivo;
        //            lbl_Status.Text = "Loading...";
        //            LeeArchivo(archivo);
        //            MueveArchivo(archivo);
        //        }

        //        streamReader.Close();
        //        response.Close();
        //    }
        //    catch(Exception x)
        //    {
        //        return;
        //    }
             
        //}
      

        //public static void DownloadFTP(string LocalDirectory, string RemoteFile,string Login, string Password)
        //{
        //    // Creo el objeto ftp
        //    FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(RemoteFile);

        //    // Fijo las credenciales, usuario y contraseña
        //    ftp.Credentials = new NetworkCredential(Login, Password);

        //    // Le digo que no mantenga la conexión activa al terminar.
        //    ftp.KeepAlive = false;
            
        //    // Indicamos que la operación es descargar un archivo...
        //    ftp.Method = WebRequestMethods.Ftp.DownloadFile;

        //    // … en modo binario … (podria ser como ASCII)
        //    ftp.UseBinary = true;

        //    // Desactivo cualquier posible proxy http.
        //    // Ojo pues de saltar este paso podría usar 
        //    // un proxy configurado en iexplorer
        //    ftp.Proxy = null;
            
        //    // Activar si se dispone de SSL
        //    ftp.EnableSsl = false;
            
        //    // Configuro el buffer a 2 KBytes
        //    int buffLength = 2048;
        //    byte[] buff = new byte[buffLength];
        //    int contentLen;

        //    LocalDirectory = Path.Combine(LocalDirectory, Path.GetFileName(RemoteFile));
        //    using (FileStream fs = new FileStream(LocalDirectory, FileMode.Create,FileAccess.Write, FileShare.None))

        //    using (Stream strm = ftp.GetResponse().GetResponseStream())
        //    {
        //        // Leer del buffer 2kb cada vez
        //        contentLen = strm.Read(buff, 0, buffLength);

        //        // mientras haya datos en el buffer...
        //        while (contentLen != 0)
        //        {
        //            // escribir en el stream del fichero
        //            //el contenido del stream de conexión
        //            fs.Write(buff, 0, contentLen);
        //            contentLen = strm.Read(buff, 0, buffLength);
        //        }
        //    }
        //}

        //public void LeeArchivo(string archivo)
        //{
        //    try
        //    {
        //        var direccion = "C:\\Loaded_Files\\" + archivo;
        //        //var direccion = "D:\\"+archivo;
        //        FileInfo existingFile = new FileInfo(direccion);
        //        using (ExcelPackage package = new ExcelPackage(existingFile))
        //        {
        //            //get the first worksheet in the workbook
        //            ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
        //            int colCount = worksheet.Dimension.End.Column;  //get Column Count
        //            int rowCount = worksheet.Dimension.End.Row;     //get row count
        //            for (int row = 1; row <= rowCount; row++)
        //            {
        //                for (int col = 1; col <= colCount; col++)
        //                {
        //                    Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + worksheet.Cells[row, col].Value?.ToString().Trim());
        //                }
        //            }

        //            //if (tbl.Columns.Count == 0)
        //            //{
        //            //    for (int col = 0; col < 33; col++)
        //            //        switch (col)
        //            //        {
        //            //            case 0: tbl.Columns.Add(new DataColumn("Rosetta_TicketId")); break;
        //            //            case 1: tbl.Columns.Add(new DataColumn("ProductCode")); break;
        //            //            case 2: tbl.Columns.Add(new DataColumn("TotalQuantity")); break;
        //            //            case 3: tbl.Columns.Add(new DataColumn("UnitMeasure")); break;
        //            //            case 4: tbl.Columns.Add(new DataColumn("User")); break;
        //            //            case 5: tbl.Columns.Add(new DataColumn("DistributorId")); break;
        //            //            case 6: tbl.Columns.Add(new DataColumn("PurchaseOrder")); break;
        //            //            case 7: tbl.Columns.Add(new DataColumn("typeofIssue")); break;
        //            //            case 8: tbl.Columns.Add(new DataColumn("lotNumber")); break;
        //            //            case 9: tbl.Columns.Add(new DataColumn("Serial")); break;
        //            //            case 10: tbl.Columns.Add(new DataColumn("caRequiered")); break;
        //            //            case 11: tbl.Columns.Add(new DataColumn("problemDescription (IssueComents)")); break;
        //            //            case 12: tbl.Columns.Add(new DataColumn("issue")); break;
        //            //            case 13: tbl.Columns.Add(new DataColumn("issueDetail")); break;
        //            //            case 14: tbl.Columns.Add(new DataColumn("Part1")); break;
        //            //            case 15: tbl.Columns.Add(new DataColumn("Descr1")); break;
        //            //            case 16: tbl.Columns.Add(new DataColumn("Part2")); break;
        //            //            case 17: tbl.Columns.Add(new DataColumn("Descr2")); break;
        //            //            case 18: tbl.Columns.Add(new DataColumn("Part3")); break;
        //            //            case 19: tbl.Columns.Add(new DataColumn("Descr3")); break;
        //            //            case 20: tbl.Columns.Add(new DataColumn("Part4")); break;
        //            //            case 21: tbl.Columns.Add(new DataColumn("Descr4")); break;
        //            //            case 22: tbl.Columns.Add(new DataColumn("Part5")); break;
        //            //            case 23: tbl.Columns.Add(new DataColumn("Descr5")); break;
        //            //            case 24: tbl.Columns.Add(new DataColumn("Part6")); break;
        //            //            case 25: tbl.Columns.Add(new DataColumn("Descr6")); break;
        //            //            case 26: tbl.Columns.Add(new DataColumn("Part7")); break;
        //            //            case 27: tbl.Columns.Add(new DataColumn("Descr7")); break;
        //            //            case 28: tbl.Columns.Add(new DataColumn("Part8")); break;
        //            //            case 29: tbl.Columns.Add(new DataColumn("Descr8")); break;
        //            //            //case 30: tbl.Columns.Add(new DataColumn("timeInFiel")); break;
        //            //            case 31: tbl.Columns.Add(new DataColumn("brand")); break;
        //            //            case 32: tbl.Columns.Add(new DataColumn("RMA")); break;
        //            //        }
        //            //    tbl.Columns.Add(new DataColumn("Estatus"));
        //            //    tbl.Columns.Add(new DataColumn("AssignedTo"));
        //            //    tbl.Columns.Add(new DataColumn("TicketName"));
        //            //    //tbl.Columns.Add(new DataColumn("Column" + (col + 1).ToString()));
        //            //}

        //            //string[] lines = System.IO.File.ReadAllLines(direccion);

        //            //foreach (string line in lines)
        //            //{
        //            //    var cols = line.Split('|');

        //            //    DataRow dr = tbl.NewRow();
        //            //    for (int cIndex = 0; cIndex < 32; cIndex++)
        //            //    {
        //            //        dr[cIndex] = cols[cIndex];
        //            //    }
        //            //    dr[34] = archivo;
        //            //    tbl.Rows.Add(dr);
        //            //}

        //            EvaluaTabla(tbl);

        //        EnviaCorreos(tbl);
        //        dataGridView1.DataSource = tbl;

        //        for (var i = 0; i <= 34; i++)
        //        {
        //            DataGridViewColumn ID_Column = dataGridView1.Columns[i];
        //            ID_Column.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        //        }

        //        DataGridViewColumn ID_Column2 = dataGridView1.Columns[34];
        //        dataGridView1.Sort(ID_Column2, ListSortDirection.Ascending);
        //        GuardaBitacoraCarga(tbl, direccion);
        //    }
        //    catch(Exception x)
        //    {
        //        return;
        //    }
        //}


        //public static void GuardaBitacoraCarga(DataTable tb, string direccion)
        //{
        //    DataView dtV = tb.DefaultView;
        //    dtV.Sort = "Estatus ASC";
        //    tb = dtV.ToTable();
        //    var bandera = 0;
        //    var bandera1 = 0;
        //    var bandera2 = 0;
        //    var bandera3 = 0;
        //    var ubicacion = direccion + ".xlsxx";
        //    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    ExcelPackage ep = new ExcelPackage(new FileInfo(ubicacion));
        //    ep.Workbook.Worksheets.Add("Import Results");

        //    ExcelWorksheet ew1 = ep.Workbook.Worksheets[1];
        //    ew1.Column(1).Style.WrapText = true;
        //    ew1.Column(2).Style.WrapText = true;
        //    ew1.Column(3).Style.WrapText = true;
        //    ew1.Column(4).Style.WrapText = true;
        //    ew1.Column(5).Style.WrapText = true;
        //    ew1.Cells["A1"].Value = "Correct Uploaded";
        //    ew1.Cells["A1:D1"].Merge = true;
        //    ew1.Cells["A1"].Style.Font.Bold = true;
        //    ew1.Cells["A1"].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //    ew1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Yellow);

        //    ew1.Cells["A2"].Value = "ClaimId";
        //    ew1.Cells["A2"].Style.Font.Bold = true;
        //    ew1.Cells["B2"].Value = "Detail";
        //    ew1.Cells["B2"].Style.Font.Bold = true;
        //    ew1.Cells["C2"].Value = "File Name SAP";
        //    ew1.Cells["C2"].Style.Font.Bold = true;
        //    ew1.Cells["D2"].Value = "Load Date";
        //    ew1.Cells["D2"].Style.Font.Bold = true;
        //    for (var i = 0; i < tb.Rows.Count; i++)
        //    {
        //        var claimId = tb.Rows[i][0].ToString();
        //        var detalle = tb.Rows[i][32].ToString();
        //        var status = tb.Rows[i][32].ToString();
        //        var file = direccion;
        //        var fecha = DateTime.Now.ToShortDateString() + "-" + DateTime.Now.ToShortTimeString();
        //        var query = "";

        //        conn.Open();
        //        query = " insert into bitacoraSap   select '" + claimId + "','" + detalle + "','" + file + "','" + fecha + "'";
        //        var cmd22 = new SqlCommand(query, conn);
        //        cmd22.ExecuteReader();
        //        conn.Close();

        //        if (status.Contains("Complete"))
        //        {
        //            ew1.Cells[(i + 3), 1].Value = claimId.ToString();
        //            ew1.Cells[(i + 3), 2].Value = detalle.ToString();
        //            ew1.Cells[(i + 3), 3].Value = file.ToString();
        //            ew1.Cells[(i + 3), 4].Value = fecha.ToString();
        //        }
        //        else
        //        {
        //            if (status.Contains("customer"))
        //            {
        //                if (bandera == 0)
        //                {
        //                    ew1.Cells[(i + 4), 1].Value = "Distributor not exist";
        //                    ew1.Cells[(i + 4), 1].Style.Font.Bold = true;
        //                    ew1.Cells[(i + 4), 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                    ew1.Cells[(i + 4), 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                    ew1.Cells[(i + 4), 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                    ew1.Cells[(i + 4), 2].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                    ew1.Cells[(i + 4), 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                    ew1.Cells[(i + 4), 3].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                    ew1.Cells[(i + 4), 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                    ew1.Cells[(i + 4), 4].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                    bandera = 1;

        //                    ew1.Cells[(i + 5), 1].Value = "ClaimId";
        //                    ew1.Cells[(i + 5), 1].Style.Font.Bold = true;
        //                    ew1.Cells[(i + 5), 2].Value = "Detail";
        //                    ew1.Cells[(i + 5), 2].Style.Font.Bold = true;
        //                    ew1.Cells[(i + 5), 3].Value = "File Name SAP";
        //                    ew1.Cells[(i + 5), 3].Style.Font.Bold = true;
        //                    ew1.Cells[(i + 5), 4].Value = "Load Date";
        //                    ew1.Cells[(i + 5), 4].Style.Font.Bold = true;
        //                }

        //                ew1.Cells[(i + 6), 1].Value = claimId.ToString();
        //                ew1.Cells[(i + 6), 2].Value = detalle.ToString();
        //                ew1.Cells[(i + 6), 3].Value = file.ToString();
        //                ew1.Cells[(i + 6), 4].Value = fecha.ToString();
        //            }
        //            else
        //            {
        //                if (status.Contains("issue"))
        //                {
        //                    if (bandera1 == 0)
        //                    {
        //                        ew1.Cells[(i + 7), 1].Value = "The issue or the issue detaill not exist";
        //                        ew1.Cells[(i + 7), 1].Style.Font.Bold = true;
        //                        ew1.Cells[(i + 7), 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                        ew1.Cells[(i + 7), 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                        ew1.Cells[(i + 7), 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                        ew1.Cells[(i + 7), 2].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                        ew1.Cells[(i + 7), 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                        ew1.Cells[(i + 7), 3].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                        ew1.Cells[(i + 7), 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                        ew1.Cells[(i + 7), 4].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                        bandera1 = 1;

        //                        ew1.Cells[(i + 8), 1].Value = "ClaimId";
        //                        ew1.Cells[(i + 8), 1].Style.Font.Bold = true;
        //                        ew1.Cells[(i + 8), 2].Value = "Detail";
        //                        ew1.Cells[(i + 8), 2].Style.Font.Bold = true;
        //                        ew1.Cells[(i + 8), 3].Value = "File Name SAP";
        //                        ew1.Cells[(i + 8), 3].Style.Font.Bold = true;
        //                        ew1.Cells[(i + 8), 4].Value = "Load Date";
        //                        ew1.Cells[(i + 8), 4].Style.Font.Bold = true;

        //                    }
        //                    ew1.Cells[(i + 9), 1].Value = claimId.ToString();
        //                    ew1.Cells[(i + 9), 2].Value = detalle.ToString();
        //                    ew1.Cells[(i + 9), 3].Value = file.ToString();
        //                    ew1.Cells[(i + 9), 4].Value = fecha.ToString();
        //                }
        //                else
        //                {
        //                    if (status.Contains("Part"))
        //                    {
        //                        if (bandera2 == 0)
        //                        {
        //                            ew1.Cells[(i + 10), 1].Value = "Part number not exist";
        //                            ew1.Cells[(i + 10), 1].Style.Font.Bold = true;
        //                            ew1.Cells[(i + 10), 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                            ew1.Cells[(i + 10), 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                            ew1.Cells[(i + 10), 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                            ew1.Cells[(i + 10), 2].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                            ew1.Cells[(i + 10), 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                            ew1.Cells[(i + 10), 3].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                            ew1.Cells[(i + 10), 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                            ew1.Cells[(i + 10), 4].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                            bandera2 = 1;
        //                            ew1.Cells[(i + 11), 1].Value = "ClaimId";
        //                            ew1.Cells[(i + 11), 1].Style.Font.Bold = true;
        //                            ew1.Cells[(i + 11), 2].Value = "Detail";
        //                            ew1.Cells[(i + 11), 2].Style.Font.Bold = true;
        //                            ew1.Cells[(i + 11), 3].Value = "File Name SAP";
        //                            ew1.Cells[(i + 11), 3].Style.Font.Bold = true;
        //                            ew1.Cells[(i + 11), 4].Value = "Load Date";
        //                            ew1.Cells[(i + 11), 4].Style.Font.Bold = true;
        //                        }

        //                        ew1.Cells[(i + 12), 1].Value = claimId.ToString();
        //                        ew1.Cells[(i + 12), 2].Value = detalle.ToString();
        //                        ew1.Cells[(i + 12), 3].Value = file.ToString();
        //                        ew1.Cells[(i + 12), 4].Value = fecha.ToString();
        //                    }
        //                    else
        //                    {
        //                        if (status.Contains("user"))
        //                        {
        //                            if (bandera3 == 0)
        //                            {
        //                                ew1.Cells[(i + 13), 1].Value = "User not exist";
        //                                ew1.Cells[(i + 13), 1].Style.Font.Bold = true;
        //                                ew1.Cells[(i + 13), 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                                ew1.Cells[(i + 13), 1].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                                ew1.Cells[(i + 13), 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                                ew1.Cells[(i + 13), 2].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                                ew1.Cells[(i + 13), 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                                ew1.Cells[(i + 13), 3].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                                ew1.Cells[(i + 13), 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
        //                                ew1.Cells[(i + 13), 4].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
        //                                bandera3 = 1;

        //                                ew1.Cells[(i + 14), 1].Value = "ClaimId";
        //                                ew1.Cells[(i + 14), 1].Style.Font.Bold = true;
        //                                ew1.Cells[(i + 14), 2].Value = "Detail";
        //                                ew1.Cells[(i + 14), 2].Style.Font.Bold = true;
        //                                ew1.Cells[(i + 14), 3].Value = "File Name SAP";
        //                                ew1.Cells[(i + 14), 3].Style.Font.Bold = true;
        //                                ew1.Cells[(i + 14), 4].Value = "Load Date";
        //                                ew1.Cells[(i + 14), 4].Style.Font.Bold = true;
        //                            }

        //                            ew1.Cells[(i + 15), 1].Value = claimId.ToString();
        //                            ew1.Cells[(i + 15), 2].Value = detalle.ToString();
        //                            ew1.Cells[(i + 15), 3].Value = file.ToString();
        //                            ew1.Cells[(i + 15), 4].Value = fecha.ToString();
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    ep.Save();
        //    SendGridMessage myMessage = new SendGridMessage();
        //    myMessage.AddCc("erzeel.jauregui@createkconsultores.com");
        //    myMessage.AddTo("scott.daubert@esab.com");
        //    myMessage.AddCc("edgar.rodriguez@createkconsultores.com");
        //    myMessage.From = new MailAddress("noreply@esabrosetta.com", " No Reply Rosetta");
        //    myMessage.Subject = "Interface Results SAP";
        //    myMessage.Html = "This file have the results of SAP interface loaded, please see details";
        //    myMessage.AddAttachment(ubicacion);

        //    var credentials = new NetworkCredential("azure_3ad7d7f52bfbab2ab38f52813222d870@azure.com", "Rodri01Edg");
        //    var transportWeb = new Web(credentials);
        //    transportWeb.DeliverAsync(myMessage);
        //}
        //public void EvaluaTabla(DataTable dt)
        //{
        //    var query = "";
        //    for (var i = 0; i < dt.Rows.Count; i++)
        //    {
        //        try
        //        {
                    
        //            var claimId = dt.Rows[i][0];
        //            var partNumberO = dt.Rows[i][1];
        //            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //            conn.Open();
        //            query= "SELECT productCode FROM dbo.Products WHERE productCode='" + partNumberO.ToString().TrimEnd(' ') + "'";  // cambio a abreviatura ya q antes estaba por nombre
        //            SqlCommand cmd = new SqlCommand(query, conn);
        //            var partNumber = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();
        //            if (partNumber == "")
        //            {
        //                conn.Open();
        //                var newPart = partNumberO.ToString().TrimStart('0');
        //                query = "SELECT productCode FROM dbo.Products WHERE productCode='" + newPart.ToString().TrimEnd(' ') + "'";  // cambio a abreviatura ya q antes estaba por nombre
        //                SqlCommand cmd2 = new SqlCommand(query, conn);
        //                partNumber = Convert.ToString(cmd2.ExecuteScalar());
        //                conn.Close();
        //                if (partNumber == "")
        //                {
        //                    dt.Rows[i][32] = "Error, the Part Number not exist -" + partNumberO.ToString();
        //                    continue;
        //                }
        //            }

        //            var totalQuantity = dt.Rows[i][2];
        //            var unitMeasureId = dt.Rows[i][3];
        //            var unitMeasureIdO = dt.Rows[i][3];
        //            conn.Open();
        //            query = "SELECT UnitId FROM dbo.Units WHERE UnitAbb='" + unitMeasureId + "'";  // cambio a abreviatura ya q antes estaba por nombre
        //            cmd = new SqlCommand(query, conn);
        //            unitMeasureId = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();
        //            if (unitMeasureId == "")
        //            {
        //                dt.Rows[i][32] = "Error, the unit of measure not exist - " + unitMeasureIdO.ToString();
        //                continue;
        //            }

        //            var userId = dt.Rows[i][4];
        //            var userIdO = dt.Rows[i][4];
        //            conn.Open();
        //            query = "SELECT UserId FROM dbo.Users WHERE Email='" + userId + "'";
        //            cmd = new SqlCommand(query, conn);
        //            userId = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();
        //            if (userId == "")
        //            {
        //                dt.Rows[i][32] = "Error, the user not exist -" + userIdO.ToString();
        //                continue;
        //            }

        //            var customerIdDist = dt.Rows[i][5].ToString();
        //            var customerIdDistO = dt.Rows[i][5].ToString();
        //            var cadena = customerIdDist.TrimStart('0');
        //            conn.Open();
        //            query = "SELECT  CustomerContactId FROM dbo.CustomerContacts WHERE name LIKE '%DEFAULT%' AND DistributorId='" + cadena + "'";
        //            cmd = new SqlCommand(query, conn);
        //            customerIdDist = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();
        //            if (customerIdDist == "")
        //            {
        //                dt.Rows[i][32] = "Error, the customer not exist -" + customerIdDistO.ToString();
        //                continue;
        //            }
        //            var distributor = dt.Rows[i][5];
        //            var purchaseOrder = dt.Rows[i][6];
        //            var typeofIssue = dt.Rows[i][7];
        //            if (Convert.ToInt32(typeofIssue) > 3)
        //            {
        //                dt.Rows[i][32] = "Error, the type of issue not exist -" + typeofIssue.ToString();
        //                continue;
        //            }

        //            var lotNumber = dt.Rows[i][8];
        //            var serialNumber = dt.Rows[i][9];
        //            var caRequiered = dt.Rows[i][10];
        //            var problemDescription = dt.Rows[i][11];
        //            var issue = dt.Rows[i][12];
        //            var issueDetail = dt.Rows[i][13];
        //            var part1 = dt.Rows[i][14].ToString();
        //            var descrip1 = dt.Rows[i][15].ToString();
        //            var part2 = dt.Rows[i][16].ToString();
        //            var descrip2 = dt.Rows[i][17].ToString();
        //            var part3 = dt.Rows[i][18].ToString();
        //            var descrip3 = dt.Rows[i][19].ToString();
        //            var part4 = dt.Rows[i][20].ToString();
        //            var descrip4 = dt.Rows[i][21].ToString();
        //            var part5 = dt.Rows[i][22].ToString();
        //            var descrip5 = dt.Rows[i][23].ToString();
        //            var part6 = dt.Rows[i][24].ToString();
        //            var descrip6 = dt.Rows[i][25].ToString();
        //            var part7 = dt.Rows[i][26].ToString();
        //            var descrip7 = dt.Rows[i][27].ToString();
        //            var part8 = dt.Rows[i][28].ToString();
        //            var descrip8 = dt.Rows[i][29].ToString();

        //           // var timeInFiel = dt.Rows[i][30];
        //            var brand = dt.Rows[i][30];
        //            var rma = dt.Rows[i][31];

        //            //query = "SELECT PB.ProductBusinessArea FROM Products P, ProductCodes PC, ProductGroups PG, ProductAreas PA, ProductsBusinessArea PB  WHERE P.ProductCodeId = PC.ProductCode AND PC.ProductGroup = PG.ProductGroup AND PG.ProductArea = PA.ProductArea AND PA.ProductBusinessArea = PB.ProductBusinessArea AND P.ProductCode ='" + partNumber + "'";
        //            query= "SELECT ProductBusinessArea FROM dbo.ProductAreas WHERE ProductArea=(SELECT ProductArea FROM dbo.ProductGroups WHERE ProductGroup=(SELECT ProductGroup FROM dbo.ProductCodes WHERE ProductCode=(SELECT TOP 1 ProductCodeId  FROM dbo.Products WHERE ProductCode ='" + partNumber + "')))";

        //            conn.Open();
        //            cmd = new SqlCommand(query, conn);
        //            var productBussinesArea = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();



        //            query = "SELECT P.Plant FROM Products P, ProductCodes PC, ProductGroups PG, ProductAreas PA, ProductsBusinessArea PB  WHERE P.ProductCodeId = PC.ProductCode AND PC.ProductGroup = PG.ProductGroup AND PG.ProductArea = PA.ProductArea AND PA.ProductBusinessArea = PB.ProductBusinessArea AND P.ProductCode ='" + partNumber + "'";

        //            conn.Open();
        //            cmd = new SqlCommand(query, conn);
        //            var plant = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();

        //            var assigedTo = "";

        //            if (productBussinesArea == "PC")
        //            {
        //                query = "SELECT FirstManager FROM  DIVISIONS WHERE PLANT =  '" + plant + "'";
        //                conn.Open();
        //                cmd = new SqlCommand(query, conn);
        //                assigedTo = Convert.ToString(cmd.ExecuteScalar());
        //                conn.Close();
        //            }

        //            if (productBussinesArea != "PC")
        //            {
        //                query = "SELECT FirstManager FROM  DIVISIONS WHERE PLANT = '" + plant + "' AND ProductBusinessArea = '" + productBussinesArea + "'";
        //                conn.Open();
        //                cmd = new SqlCommand(query, conn);
        //                assigedTo = Convert.ToString(cmd.ExecuteScalar());
        //                conn.Close();
        //            }
        //            dt.Rows[i][33] = assigedTo;
        //            //division
        //            query = "SELECT divisionId FROM  DIVISIONS WHERE PLANT = '" + plant + "' AND ProductBusinessArea = '" + productBussinesArea + "'";
        //            conn.Open();
        //            cmd = new SqlCommand(query, conn);
        //            var divisionId = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();

        //            //errorReport
        //            query = "SELECT top 1 PA.ProductAreaName FROM  ProductCodes PC, ProductGroups PG ,ProductAreas PA, ProductsBusinessArea PBA WHERE Pc.ProductGroup = pg.ProductGroup AND PG.ProductArea = PA.ProductArea AND PA.ProductBusinessArea = PBA.ProductBusinessArea AND PC.ProductCode = (SELECT top 1 ProductCodeId FROM dbo.Products WHERE ProductCode = '" + partNumber + "')";
        //            conn.Open();
        //            cmd = new SqlCommand(query, conn);
        //            var productAreaName = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();


        //            //query = "SELECT ReportedErrorId FROM dbo.ReportedErrors WHERE SecondList='" + @issue + "' and ThirdList = '" + @issueDetail + "' and firstList='" + productAreaName + "' and BussinessArea='" + productBussinesArea + "'";
        //            //query = "SELECT ReportedErrorId FROM dbo.ReportedErrors WHERE SecondList='" + @issue + "' and ThirdList = '" + @issueDetail + "' and BussinessArea='" + productBussinesArea + "'";
        //            conn.Open();
        //            query = "SELECT ProductAreaName FROM ProductAreas WHERE ProductArea = (SELECT ProductArea FROM ProductGroups WHERE ProductGroup = (SELECT ProductGroup FROM ProductCodes WHERE ProductCode = (SELECT ProductCodeId FROM Products WHERE ProductCode = '" + partNumber + "')))";
        //            cmd = new SqlCommand(query, conn);
        //            var ProductAreaName2 = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();
        //            if (ProductAreaName2 == "")
        //            {
        //                dt.Rows[i][32] = "Error, the ProductAreaName not exist -" + partNumber.ToString();
        //                continue;
        //            }

        //            conn.Open();
        //            //query = "SELECT ReportedErrorId FROM dbo.ReportedErrors WHERE FirstList='" + @ProductAreaName2 + "' and ThirdList = '" + @issueDetail + "'";
        //            //cmd = new SqlCommand(query, conn);
        //            //var reportedErrorId = Convert.ToString(cmd.ExecuteScalar());


        //            var nuevoError = issue.ToString().Trim() + issueDetail.ToString().Trim();
        //            query = "SELECT ReportedReasonCodeId FROM dbo.ReportedReasonCodes WHERE ReportedReasonCodeId='" + nuevoError + "'";
        //            cmd = new SqlCommand(query, conn);
        //            var reportedErrorId = Convert.ToString(cmd.ExecuteScalar());
        //            conn.Close();

        //            if (reportedErrorId == "")
        //            {
        //                dt.Rows[i][32] = "Error, the issue or the issue detaill not exist -" + nuevoError.ToString();
        //                continue;
        //            }


        //            var newProble = @problemDescription.ToString().Replace("'", "");
        //            var mes = DateTime.Now.Month;
        //            var dia = DateTime.Now.Day;
        //            var ano = DateTime.Now.Year;
        //            var hora = DateTime.Now.ToShortTimeString();

        //            var fecha = mes + "/" + dia + "/" + ano ;
        //            //generar el insert
        //            conn.Open();
        //            //query = " insert into ticket  select '" + claimId + "'," + distributor + "," + customerIdDist + "," + divisionId + ",'" + partNumber + "'," + userId + "," + typeofIssue + ",2,'" + fecha + "',0," + assigedTo + ",'" + fecha + "',1, ' ',0,'" + purchaseOrder + "'," + caRequiered + ",'','" + serialNumber + "','" + serialNumber + "'," + totalQuantity + ",null,null,null,null,null,null,'" + newProble + "',null,null,null,'DCEP',null,null,null, null,null,null,null,null,null,null,null, null,0,null,null,null,null, null,1,null,null,null, null,0,0," + unitMeasureId + "," + reportedErrorId +       ",null, null,null,0,null,0,null,0, null,0,0, 2,null,0,null, null,null,null,null,2";
        //            query = " insert into ticket  select '" + claimId + "'," + distributor + "," + customerIdDist + "," + divisionId + ",'" + partNumber + "'," + userId + "," + typeofIssue + ",2,'" + fecha + "',0," + assigedTo + ",'" + fecha + "',0,'"+rma+"' ,0,'" + purchaseOrder + "'," + caRequiered + ",'','" + serialNumber + "','" + serialNumber + "'," + totalQuantity + ",null,null,null,null,null,null,'" + newProble + "',null,null,null,'DCEP',null,null,null, null,null,null,null,null,null,null,null, null,0,null,null,null,null, null,1,null,null,null, null,0,0," + unitMeasureId + ",'" + reportedErrorId + "',null, null,null,0,null,0,null,0, null,0,0,2,null,0,null, null,null,null,null,2";
        //            var cmd1 = new SqlCommand(query, conn);
        //            cmd1.ExecuteReader();
        //            conn.Close();


        //            conn.Open();
        //            //query = " insert into ticket  select '" + claimId + "'," + distributor + "," + customerIdDist + "," + divisionId + ",'" + partNumber + "'," + userId + "," + typeofIssue + ",2,'" + fecha + "',0," + assigedTo + ",'" + fecha + "',1, ' ',0,'" + purchaseOrder + "'," + caRequiered + ",'','" + serialNumber + "','" + serialNumber + "'," + totalQuantity + ",null,null,null,null,null,null,'" + newProble + "',null,null,null,'DCEP',null,null,null, null,null,null,null,null,null,null,null, null,0,null,null,null,null, null,1,null,null,null, null,0,0," + unitMeasureId + "," + reportedErrorId +       ",null, null,null,0,null,0,null,0, null,0,0, 2,null,0,null, null,null,null,null,2";
        //            query = " insert into AnalysisDetail (TicketId,DateRGShippedToSite)  select '" + claimId + "','1900-01-01 00:00:00.000'";
        //            var cmd22 = new SqlCommand(query, conn);
        //            cmd22.ExecuteReader();
        //            conn.Close();

        //            //part detail
        //            try
        //            {

        //                for (var j = 1; j <= 8; j++)
        //                {
        //                    conn.Open();
        //                    switch (j)
        //                    {
        //                        case 1: if (!String.IsNullOrWhiteSpace(part1)) query = "insert into PartSendRepair select '" + claimId + "','" + part1 + "','" + descrip1 + "'"; break;
        //                        case 2: if (!String.IsNullOrWhiteSpace(part2)) query = "insert into PartSendRepair select '" + claimId + "','" + part2 + "','" + descrip2 + "'"; break;
        //                        case 3: if (!String.IsNullOrWhiteSpace(part3)) query = "insert into PartSendRepair select '" + claimId + "','" + part3 + "','" + descrip3 + "'"; break;
        //                        case 4: if (!String.IsNullOrWhiteSpace(part4)) query = "insert into PartSendRepair select '" + claimId + "','" + part4 + "','" + descrip4 + "'"; break;
        //                        case 5: if (!String.IsNullOrWhiteSpace(part5)) query = "insert into PartSendRepair select '" + claimId + "','" + part5 + "','" + descrip5 + "'"; break;
        //                        case 6: if (!String.IsNullOrWhiteSpace(part6)) query = "insert into PartSendRepair select '" + claimId + "','" + part6 + "','" + descrip6 + "'"; break;
        //                        case 7: if (!String.IsNullOrWhiteSpace(part7)) query = "insert into PartSendRepair select '" + claimId + "','" + part7 + "','" + descrip7 + "'"; break;
        //                        case 8: if (!String.IsNullOrWhiteSpace(part8)) query = "insert into PartSendRepair select '" + claimId + "','" + part8 + "','" + descrip8 + "'"; break;
        //                    }

        //                    if(query!="")
        //                    {
        //                        cmd1 = new SqlCommand(query, conn);
        //                        cmd1.ExecuteReader();
        //                    }
                           
        //                    conn.Close();
        //                    query = "";
        //                }
        //            }
        //            catch (Exception x)
        //            {
        //                var z = x.Message;
        //                dt.Rows[i][32] = "Error, PartSendRepair " + query;
        //                conn.Close();
        //            }
        //            conn.Close();
        //            dt.Rows[i][32] = "Complete";
                   
        //        }
        //        catch (Exception x)
        //        {
        //            var z = x.Message;
        //            dt.Rows[i][32] = "Error, the information is not correct. "+z.ToString()+" "+query;
                    
        //        }
        //    }
        //}

        //public void EnviaCorreos(DataTable dt)
        //{
        //    DataView dv = dt.DefaultView;
        //    dv.Sort = "AssignedTo asc";
        //    DataTable sortedDT = dv.ToTable();
        //    var valorActual = "";
        //    var cuerpo = "Below tickets has been assigned to you: <br><br> <table border='1' bordercolor='black' ><tr style='font-weight: bold;'><td> Ticket ID </td> <td> Ticket Owner </td><td>Part Number</td><td> Description </td><td> Qty </td><td> Issue </td><td> Issue Detail </td> <td> View Details </td></tr>";
        //    var total = sortedDT.Rows.Count-1;
         
        //    for (var i = 0; i<sortedDT.Rows.Count; i++)
        //    {
        //        var asiggnedTo = sortedDT.Rows[i]["AssignedTo"].ToString();
        //        var estatus = sortedDT.Rows[i]["Estatus"].ToString();
        //        var partNumber = sortedDT.Rows[i]["ProductCode"].ToString();
        //        var description = sortedDT.Rows[i]["problemDescription (IssueComents)"].ToString();
        //        var qty = sortedDT.Rows[i]["TotalQuantity"].ToString();
        //        var issue = sortedDT.Rows[i]["issue"].ToString();
        //        var issueDetail = sortedDT.Rows[i]["issueDetail"].ToString();
        //        if (!estatus.Contains("Error"))
        //        {
        //            if (asiggnedTo != "")
        //            {
        //                if (valorActual == "")
        //                {
        //                    valorActual = asiggnedTo;
        //                }

        //                if (valorActual != asiggnedTo)//si es otro
        //                {
        //                    var query = "SELECT email FROM users WHERE userid=" + valorActual;
        //                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //                    conn.Open();
        //                    SqlCommand cmd = new SqlCommand(query, conn);
        //                    var emailAsignado = Convert.ToString(cmd.ExecuteScalar());
        //                    conn.Close();


        //                    query = "SELECT userName FROM users WHERE email='" + sortedDT.Rows[i][4].ToString() + "'";
        //                    SqlConnection conn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //                    conn.Open();
        //                    SqlCommand cmd2 = new SqlCommand(query, conn);
        //                    var creado = Convert.ToString(cmd.ExecuteScalar());
        //                    conn.Close();



        //                    //Envia el correo ya formado
        //                    SendGridMessage myMessage = new SendGridMessage();
        //                    //myMessage.AddCc("scott.daubert@esab.com");
        //                    //myMessage.AddCc("rodolfo.galicia14@gmail.com");
        //                    myMessage.AddTo(emailAsignado);
        //                    //myMessage.AddCc("era521@hotmail.com");
        //                    myMessage.From = new MailAddress("noreply@esabrosetta.com", " No Reply Rosetta");
        //                    myMessage.Subject = "SAP Claim Assignation";
        //                    myMessage.Html = cuerpo;
        //                    myMessage.Html = cuerpo + "</table>";

        //                    var credentials = new NetworkCredential("azure_3ad7d7f52bfbab2ab38f52813222d870@azure.com", "Rodri01Edg");
        //                    var transportWeb = new Web(credentials);
        //                    transportWeb.DeliverAsync(myMessage);

        //                    valorActual = asiggnedTo;

        //                    cuerpo = "Below tickets has been assigned to you: <br><br> <table border='2px'><tr style='font-weight: bold;'><td> Ticket ID </td> <td> Ticket Owner </td><td> Part Number </td><td> Description </td><td> Qty </td><td> Issue </td><td> Issue Detail </td> <td> View Details </td></tr>";
        //                    var ticket = sortedDT.Rows[i][0].ToString();
        //                    cuerpo += "<tr><td>" + ticket + "</td><td>" + creado + "</td><td>" + partNumber + "</td><td>" + description + "</td><td>" + qty + "</td><td>" + issue + "</td><td>" + issueDetail + "</td><td><a href='http://esabrosetta.azurewebsites.net/Ticket2.0.aspx?rosetta=" + ticket + "'>" + "<center>View</center></a></td></tr>";
        //                }
        //                else // si es el mismo
        //                {
        //                    var query = "SELECT userName FROM users WHERE email='" + sortedDT.Rows[i][4].ToString() + "'";
        //                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //                    conn.Open();
        //                    SqlCommand cmd2 = new SqlCommand(query, conn);
        //                    var creado = Convert.ToString(cmd2.ExecuteScalar());
        //                    conn.Close();

        //                    var ticket = sortedDT.Rows[i][0].ToString();
        //                    cuerpo += "<tr><td>" + ticket + "</td><td>" + creado + "</td><td>" + partNumber + "</td><td>" + description + "</td><td>" + qty + "</td><td>" + issue + "</td><td>" + issueDetail + "</td><td><a href='http://esabrosetta.azurewebsites.net/Ticket2.0.aspx?rosetta=" + ticket + "'>" + "<center>View</center></a></td></tr>";
        //                    if (i == total)
        //                    {
        //                        query = "SELECT email FROM users WHERE userid=" + valorActual;
        //                        SqlConnection conn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //                        conn2.Open();
        //                        SqlCommand cmd = new SqlCommand(query, conn2);
        //                        var emailAsignado = Convert.ToString(cmd.ExecuteScalar());
        //                        conn2.Close();

        //                        //Envia el correo ya formado
        //                        SendGridMessage myMessage = new SendGridMessage();
        //                        //myMessage.AddTo("edgar.j.rodriguez@outlook.com");
        //                        //myMessage.AddCc("scott.daubert@esab.com");
        //                        //myMessage.AddCc("rodolfo.galicia14@gmail.com");
        //                        myMessage.AddTo(emailAsignado);
        //                        //myMessage.AddCc("era521@hotmail.com");
        //                        myMessage.From = new MailAddress("noreply@esabrosetta.com", " No Reply Rosetta");
        //                        myMessage.Subject = "SAP Claim Assignation";
        //                        //myMessage.Html = cuerpo;
        //                        myMessage.Html = cuerpo + "</table>";

        //                        var credentials = new NetworkCredential("azure_3ad7d7f52bfbab2ab38f52813222d870@azure.com", "Rodri01Edg");
        //                        var transportWeb = new Web(credentials);
        //                        transportWeb.DeliverAsync(myMessage);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

       
    }
}
